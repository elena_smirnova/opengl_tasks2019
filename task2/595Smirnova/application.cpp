#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"

#include "perlin_noise.h"
#include "on_surface_camera.h"
#include "common/Texture.hpp"
#include "common/LightInfo.hpp"

#include <iostream>
#include <vector>
#include <fstream>

MeshPtr makeSurface(unsigned int N = 100, int size = 10, int h = 10)
{
    std::ifstream input("/home/elena/compgraphics/t/opengl_tasks2019/task2/595Smirnova/perlin.txt");

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    for( unsigned int i = 0; i < N * size; i++ )
    {
        for( unsigned int j = 0; j < N * size; j++ )
        {
            float h1, h2, h3;
            input >> h1 >> h2 >> h3;
            glm::vec3 x = glm::vec3((float)i / N * size, (float)j / N * size, h1* h);
            glm::vec3 y = glm::vec3((float)(i + 1) / N * size, (float)j / N * size, h2 * h);
            glm::vec3 z = glm::vec3((float)i / N * size, (float)(j + 1) / N * size, h3 * h);


            vertices.push_back(x);
            vertices.push_back(y);
            vertices.push_back(z);

            glm::vec3 normal = glm::normalize(glm::cross(x - y, x - z));
            normals.push_back(normal);
            normals.push_back(normal);
            normals.push_back(normal);

            texcoords.push_back(glm::vec2((float)i / N / size, (float)j / N / size));
            texcoords.push_back(glm::vec2((float)(i + 1) / N / size, (float)j / N / size));
            texcoords.push_back(glm::vec2((float)i / N / size, (float)(j + 1) / N / size));

            input >> h1 >> h2 >> h3;

            glm::vec3 a = glm::vec3((float)(i + 1) / N * size, (float)j / N * size, h1 * h);
            glm::vec3 b = glm::vec3((float)i / N * size, (float)(j + 1) / N * size, h2 * h);
            glm::vec3 c = glm::vec3((float)(i + 1) / N * size, (float)(j + 1) / N * size, h3 * h);

            vertices.push_back(a);
            vertices.push_back(b);
            vertices.push_back(c);

            glm::vec3 normal2 = -1.0f * glm::normalize(glm::cross(a - b, a - c));
            normals.push_back(normal2);
            normals.push_back(normal2);
            normals.push_back(normal2);

            texcoords.push_back(glm::vec2((float)(i + 1) / N / size, (float)j / N / size));
            texcoords.push_back(glm::vec2((float)i / N / size, (float)(j + 1) / N / size));
            texcoords.push_back(glm::vec2((float)(i + 1) / N / size, (float)(j + 1) / N / size));
        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Surface is created with " << vertices.size() << " vertices\n";

    return mesh;
}


class SampleApplication : public Application
{
  public:
    MeshPtr _surface;
    MeshPtr _marker;

    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;

    TexturePtr _worldTexture, _sandTexture, _grassTexture, _gravelTexture, _snowTexture;

    GLuint _samplerTexture0;
    GLuint _samplerTexture1;
    GLuint _samplerTexture2;
    GLuint _samplerTexture3;
    GLuint _samplerTexture4;

    LightInfo _light;

    float _lr = 15.0f;
    float _phi = 7.5f;
    float _theta = 7.5f;

    void makeScene() override
    {
        Application::makeScene();

        _surface = makeSurface(100, 10, 7);
        _surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
        _marker = makeSphere(0.5f);

        _markerShader = std::make_shared<ShaderProgram>("595SmirnovaData2/marker.vert", "595SmirnovaData2/marker.frag");

        _shader = std::make_shared<ShaderProgram>("595SmirnovaData2/texture.vert", "595SmirnovaData2/texture.frag");
        _cameraMover = std::make_shared<OnSurfaceCameraMover>(100, 10, 7);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);


        _worldTexture = loadTexture("595SmirnovaData2/export.png", SRGB::YES);
        _sandTexture = loadTexture("595SmirnovaData2/sand.jpg");
        _grassTexture = loadTexture("595SmirnovaData2/grass.jpg");
        _gravelTexture = loadTexture("595SmirnovaData2/gravel.jpg");
        _snowTexture = loadTexture("595SmirnovaData2/snow.jpg");

        glGenSamplers(1, &_samplerTexture0);
        glSamplerParameteri(_samplerTexture0, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerTexture0, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerTexture0, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_samplerTexture0, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_POLYGON_OFFSET_FILL);

        glGenSamplers(1, &_samplerTexture1);
        glSamplerParameteri(_samplerTexture1, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerTexture1, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerTexture1, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_samplerTexture1, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_POLYGON_OFFSET_FILL);

        glGenSamplers(1, &_samplerTexture2);
        glSamplerParameteri(_samplerTexture2, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerTexture2, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerTexture2, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_samplerTexture2, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_POLYGON_OFFSET_FILL);

        glGenSamplers(1, &_samplerTexture3);
        glSamplerParameteri(_samplerTexture3, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerTexture3, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerTexture3, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_samplerTexture3, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_POLYGON_OFFSET_FILL);

        glGenSamplers(1, &_samplerTexture4);
        glSamplerParameteri(_samplerTexture4, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerTexture4, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerTexture4, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_samplerTexture4, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_POLYGON_OFFSET_FILL);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 40.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 4.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f,4.0f * glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_LINE_SMOOTH);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменные: время и матрицы
        _shader->setFloatUniform("time", (float)glfwGetTime()); //передаем время в шейдер


        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shader->setVec3Uniform("light.pos", lightPosCamSpace);
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        glBindSampler(0, _samplerTexture0);
        glActiveTexture(GL_TEXTURE0);
        _worldTexture->bind();
        _shader->setIntUniform("diffuseTex", 0);

        glActiveTexture(GL_TEXTURE1);
        glBindSampler(1, _samplerTexture1);
        _sandTexture->bind();
        _shader->setIntUniform("sandTexture", 1);

        glActiveTexture(GL_TEXTURE2);
        glBindSampler(2, _samplerTexture2);
        _grassTexture->bind();
        _shader->setIntUniform("grassTexture", 2);

        glActiveTexture(GL_TEXTURE3);
        glBindSampler(3, _samplerTexture3);
        _gravelTexture->bind();
        _shader->setIntUniform("gravelTexture", 3);

        glActiveTexture(GL_TEXTURE4);
        glBindSampler(4, _samplerTexture4);
        _snowTexture->bind();
        _shader->setIntUniform("snowTexture", 4);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix",
                                glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _surface->modelMatrix()))));
        _surface->draw();
        //Рисуем маркер для источника света

        _markerShader->use();
        _markerShader->setMat4Uniform("mvpMatrix",
                                      _camera.projMatrix * _camera.viewMatrix
                                          * glm::translate(glm::mat4(1.0f), _light.position));
        _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
        _marker->draw();

        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

void write_perlin_noise(int N = 100, int size = 10)
{
    std::ofstream output("/home/elena/compgraphics/t/opengl_tasks2019/task2/595Smirnova/perlin.txt");
    if ( !output.is_open() )
    {
        std::cout << "error";
    }
    PerlinNoise pn(237);
    for( unsigned int i = 0; i < N * size; i++ )
    {
        for( unsigned int j = 0; j < N * size; j++ ) {
            output << pn.noise((float) i / N, (float) j / N, 0) << " ";
            output << pn.noise((float) (i + 1) / N, (float) j / N, 0) << " ";
            output << pn.noise((float) i / N, (float) (j + 1) / N, 0) << " ";

            output << pn.noise((float) (i + 1) / N, (float) j / N, 0) << " ";
            output << pn.noise((float) i / N, (float) (j + 1) / N, 0) << " ";
            output << pn.noise((float) (i + 1) / N, (float) (j + 1) / N, 0) << " ";
        }
    }

    output.close();

}

int main()
{
    SampleApplication app;
    write_perlin_noise(100, 10);
    app.start();
    return 0;
}