//
// Created by elena on 17.04.19.
//

#include "on_surface_camera.h"

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <imgui.h>

#include <fstream>
#include <iostream>

OnSurfaceCameraMover::OnSurfaceCameraMover(int N, int size, int h) :
    CameraMover(),
    _pos(5.0f, 0.0f, 2.5f), N(N), size(size), h(h)
{
    std::ifstream input("/home/elena/compgraphics/t/opengl_tasks2019/task2/595Smirnova/perlin.txt");

    for( unsigned int i = 0; i < N * size; i++ )
    {
        for( unsigned int j = 0; j < N * size; j++ )
        {
            float h1, h2, h3;
            input >> h1 >> h2 >> h3;
            vertices.insert( std::make_pair( std::make_pair(i, j), h1 * h));
            vertices.insert( std::make_pair( std::make_pair(i + 1, j), h2 * h));
            vertices.insert( std::make_pair( std::make_pair(i, j + 1), h3 * h));

            input >> h1 >> h2 >> h3;

            vertices.insert( std::make_pair( std::make_pair(i + 1, j + 1), h3 * h));
        }
    }
    //Нам нужно как-нибудь посчитать начальную ориентацию камеры
    _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(10.0f, 10.0f, 1.0f), glm::vec3(0.0f, 0.0f, 1.0f)));
}

void OnSurfaceCameraMover::handleKey(GLFWwindow *window, int key, int scancode, int action, int mods) {

}


void OnSurfaceCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS)
    {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        //Добавляем небольшой поворот вверх/вниз
        glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
        _rot *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

        //Добавляем небольшой поворов вокруг вертикальной оси
        glm::vec3 upDir(0.0f, 0.0f, 1.0f);
        _rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void OnSurfaceCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void OnSurfaceCameraMover::update(GLFWwindow* window, double dt)
{
    float speed = 15.0f;

    //Получаем текущее направление "вперед" в мировой системе координат
    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

    //Получаем текущее направление "вправо" в мировой системе координат
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

    //Двигаем камеру вперед/назад
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        _pos += forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        _pos -= forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        _pos -= rightDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        _pos += rightDir * speed * static_cast<float>(dt);
    }

    //-----------------------------------------

    int i = (_pos.x ) * N / size;
    int j = (_pos.y ) * N / size;
    if ( vertices.find(std::make_pair(i, j)) != vertices.end())
    {
        _pos.z = vertices.at(std::make_pair(i, j)) + 1.0f;
    }

//    _pos.z = 15.0f;
    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}
